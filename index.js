let http = require('http');

let port = 8000;

let server = http.createServer(function(request,response){
	if(request.url == '/register'){
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("You are now in Regiter Route");
	}

	else if(request.url == '/homepage'){
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("You are now in Homepage Route");
	}

	else if(request.url == '/course'){
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("You are now in Course Route");
	}

	else if(request.url == '/userProfile'){
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("You are now in User Profile Route");
	}
	else {
		response.writeHead(404, {'Content-Type' : 'text/plain'});
		response.end("I'm sorry the page you are looking for cannot be found");
	}




});
server.listen(port);
console.log(`Server is now running at localhost ${port}`);